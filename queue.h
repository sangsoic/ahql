/**
 * \file queue.h
 * \brief generic queue library allowing to manage queues generically.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-03-25 Wed 10:37 PM
 * \copyright
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __QUEUE_H__
	#define __QUEUE_H__

	#include <stdio.h>
	#include <stdlib.h>
	#include <errno.h>
	#include <string.h>
	#include <stdbool.h>

	/**
	 * \def QUEUE
	 * \brief User-level working objects.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define QUEUE(TYPE, NAME) \
		typedef struct QueueElement##NAME { \
			TYPE * value; \
			struct QueueElement##NAME * next; \
		} QueueElement##NAME; \
		typedef struct { \
			QueueElement##NAME * head; \
			QueueElement##NAME * tail; \
			size_t size; \
		} Queue##NAME;

	/**
	 * \def QUEUE_MALLOC
	 * \brief Wrapper routine for \a Queue_malloc routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define QUEUE_MALLOC(TYPE, NAME) \
		static Queue##NAME * Queue##NAME##_malloc(void) { \
			return (Queue##NAME *)Queue_malloc(); \
		}

	/**
	 * \def QUEUE_ISEMPTY
	 * \brief Wrapper routine for \a Queue_isempty routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define QUEUE_ISEMPTY(TYPE, NAME) \
		static bool Queue##NAME##_isempty(const Queue##NAME * const queue) \
		{ \
			return Queue_isempty((Queue *)queue); \
		}

	/**
	 * \def QUEUE_AT_HEAD
	 * \brief Wrapper routine for \a Queue_at_head routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define QUEUE_AT_HEAD(TYPE, NAME) \
		static QueueElement##NAME * Queue##NAME##_at_head(const Queue##NAME * const queue) \
		{ \
			return (QueueElement##NAME *)Queue_at_head((Queue *)queue); \
		}

	/**
	 * \def QUEUE_SET_HEAD
	 * \brief Wrapper routine that sets the data of the first element.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define QUEUE_SET_HEAD(TYPE, NAME) \
		static void Queue##NAME##_set_head(const Queue##NAME * const queue, const TYPE value) \
		{ \
			*((TYPE *)(Queue_at_head((Queue *)queue)->value)) = value; \
		}

	/**
	 * \def QUEUE_GET_HEAD
	 * \brief Wrapper routine that returns the data of the first element.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define QUEUE_GET_HEAD(TYPE, NAME) \
		static TYPE Queue##NAME##_get_head(const Queue##NAME * const queue) \
		{ \
			return *((TYPE *)(Queue_at_head((Queue *)queue)->value)); \
		}

	/**
	 * \def QUEUE_SWAP
	 * \brief Wrapper routine for \a Queue_swap routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define QUEUE_SWAP(TYPE, NAME) \
		static void Queue##NAME##_swap(Queue##NAME * const queue0, Queue##NAME * const queue1) \
		{ \
			Queue_swap((Queue *)queue0, (Queue *)queue1); \
		}

	/**
	 * \def QUEUE_ENQUEUE
	 * \brief Wrapper routine for \a Queue_enqueue routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define QUEUE_ENQUEUE(TYPE, NAME) \
		static void Queue##NAME##_enqueue(Queue##NAME * const queue, const TYPE value) \
		{ \
			Queue_enqueue((Queue *)queue, &value, sizeof(TYPE)); \
		}

	/**
	 * \def QUEUE_DEQUEUE
	 * \brief Wrapper routine for \a Queue_dequeue routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define QUEUE_DEQUEUE(TYPE, NAME) \
		static void Queue##NAME##_dequeue(Queue##NAME * const queue) \
		{ \
			Queue_dequeue((Queue *)queue); \
		}

	/**
	 * \def QUEUE_FREE
	 * \brief Wrapper routine for \a Queue_free routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define QUEUE_FREE(TYPE, NAME) \
		static void Queue##NAME##_free(Queue##NAME * * const queue) \
		{ \
			Queue_free((Queue * *)queue); \
		} 

	/**
	* \struct QueueElement
	* \brief Represents a Queue element object type.
	* \author Sangsoic <sangsoic@protonmail.com>
	* \version 0.1
	* \date 2020-03-25 Wed 11:55 PM
	* \var void * value
	* Address of the generic data.
	* \var QueueElement * next
	* Address of the next queue element.
	*/
	typedef struct QueueElement {
		void * value;
		struct QueueElement * next;
	} QueueElement;

	/**
	* \struct Queue
	* \brief Represents a Queue object type.
	* \author Sangsoic <sangsoic@protonmail.com>
	* \version 0.1
	* \date 2020-03-25 Wed 11:56 PM 
	* \var QueueElement * head
	* Address of the head sentinel.
	* \var QueueElement * tail
	* Address of the last element.
	* \var size_t size
	* Number of element contained within the queue.
	*/
	typedef struct {
		QueueElement * head;
		QueueElement * tail;
		size_t size;
	} Queue;

	/**
	 * \fn Queue * Queue_malloc(void)
	 * \brief Allocates queue objects.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 10:45 PM
	 * \return Address of the newly allocated Queue object;
	 */
	Queue * Queue_malloc(void);

	/**
	 * \fn bool Queue_isempty(const Queue * const queue)
	 * \brief Tests if a given queue is empty.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 10:50 PM
	 * \param queue A given queue.
	 * \return true if queue is empty else false.
	 */
	bool Queue_isempty(const Queue * const queue);

	/**
	 * \fn QueueElement * Queue_at_head(const Queue * const queue)
	 * \brief Returns the address of the first element.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 10:52 PM
	 * \param queue A given queue.
	 * \return Address of the first element;
	 */
	QueueElement * Queue_at_head(const Queue * const queue);

	/**
	 * \fn void Queue_swap(Queue * const queue0, Queue * const queue1)
	 * \brief Swaps the content of two given queues.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 10:55 PM
	 * \param queue0 A given queue.
	 * \param queue1 A given queue.
	 */
	void Queue_swap(Queue * const queue0, Queue * const queue1);

	/**
	 * \fn void Queue_enqueue(Queue * const queue, const void * const value, const size_t offset)
	 * \brief Adds a given element at the back of the queue.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 10:57 PM
	 * \param queue A given queue.
	 * \param value The address of the value to add in the queue.
	 * \param offset Size in byte of the given value.
	 */
	void Queue_enqueue(Queue * const queue, const void * const value, const size_t offset);
	
	/**
	 * \fn void Queue_dequeue(Queue * const queue)
	 * \brief Removes the first element of the queue.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 11:01 PM
	 * \param queue A given queue.
	 */
	void Queue_dequeue(Queue * const queue);

	/**
	 * \fn void Queue_free(Queue * * const queue)
	 * \brief Frees memory taken by a given queue.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.2
	 * \date 2020-03-25 Wed 11:03 PM
	 * \param queue Address of the pointer containing the queue to be freed.
	 */
	void Queue_free(Queue * * const queue);

#endif
